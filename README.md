##Média
Média é o valor médio de uma distribuição. Ela é utilizada para representar todos os valores da distribuição.

##Média aritmética simples
Média aritmética simples é obtida dividindo-se a soma das observações pelo número delas.

##Média aritmética ponderada
A média aritmética ponderada desses n números é a soma dos produtos de cada um multiplicados por seus respectivos pesos, dividida pela soma dos pesos.

##Média cúbica
Média cúbica é definida como a raiz cúbica da média aritmética dos cubos de uma seqüência finita de números reais. É o caso particular de grau 3 da média generalizada.

##Média quadrática
A raiz quadrada da média aritmética de uma quantidade finita de valores quadráticos é chamada média quadrática.

##Média harmônica
A média harmônica, H, dos números reais positivos x1,…,xn > 0 é definida como o número de membros dividido pela soma do inverso dos membros.

##Média geométrica
Média geométrica de um conjunto de números positivos é definida como o produto de todos os membros do conjunto elevado ao inverso do número de membros. Indica a tendência central ou o valor típico de um conjunto de números usando o produto dos seus valores.

##Referência
[1] Média, wikipedia, https://pt.wikipedia.org/wiki/M%C3%A9dia