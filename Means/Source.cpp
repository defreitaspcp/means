/*
Author: de Freitas, P.C.P
Description -  Mean 
References
[1] M�dia, wikipedia, https://pt.wikipedia.org/wiki/M%C3%A9dia
*/
#include<iostream>
#include<cmath>
using namespace std;
double arithmeticMean(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double geometricMean(double data[], int length)
{
	double production = 1;;
	for (size_t i = 0; i < 5; i++)
	{
		production = production * data[i];
	}
	return pow(production, 0.2);
}
double harmonicMean(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += (1 / data[i]);
	}
	return length / sum;
}
double quadraticMean(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += pow(data[i], 2);
	}
	return pow((sum / length), 0.5);
}
double cubicMean(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += pow(data[i], 3);
	}
	return pow((sum / length), 0.33);
}
double weightedArithmeticMean(double data[], double weight[], int length)
{
	double production = 1;
	double sumWeight = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		production += data[i] * weight[i];
		sumWeight += weight[i];
	}
	return production / sumWeight;
}
int main()
{
	double v[5] = { 4, 36, 45, 50, 75 };
	double w[5] = { 0.1,0.2,0.3,0.4,0.5 };
	cout << "Arithmetic Mean: " <<arithmeticMean(v, 5) << endl;
	cout << "Weighted Arithmetic Mean: " << weightedArithmeticMean(v, w, 5) << endl;
	cout << "Cubic Mean: " << cubicMean(v,5) << endl;
	cout << "Quadratic Mean: " << quadraticMean(v, 5) << endl;
	cout << "Harmonic Mean: " << harmonicMean(v, 5) << endl;
	cout << "Geometric Mean: " << geometricMean(v, 5) << endl;
	return 0;
}